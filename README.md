# Context
This was a cool challenge!

I took the opportunity to work with different technologies and play around with
an architecture that should be easily scalable - despite the fact that I didn't
setup the project with multiple service instances and a load balancer in time.
It might look like an unnecessary complexity, but I decided to use this
challenge to have some fun :)

With the data model in hands, the development of the API service was pretty
straightforward. As I wanted to optimize time and have a data source right away,
my decision was to create a Rails API with a single endpoint for posting new
reviews.

For the search functionality I decided to use Elasticsearch as engine. This decision
was taken because I was mentioned that the company uses it in real life and it's
been some time since I last used it.

As I was willing to use Kafka with Golang for some time, I used it to create the
reviews indexer. The consumer part of the code was pretty straightforward but
the biggest challenge of the whole project came as a consequence of this
decision: using Elasticsearch in a Golang service is not an easy task taking
into consideration the limited amount of time I had.

After messing around with different clients and different ES versions, it
finally worked but the complexity of the code it generated is far from what I
consider ideal. After so much time spent, my goal became "make it work" from the
initial "let's make it beautiful". Test coverage was one of the aspects that
suffered a lot from this decision.

As the indexing worked, I used the `repository` pattern to implement the
`search` repository for indexing the reviews, creating the query service relying
on the same structure was a no brainer.
The service is extremely simple and specific: it receives a query string via HTTP
request, generates a ES query and returns its response.

Finally, the Dashboard is a React app - far from ideal. The charts were created
using Highcharts as suggested (another challenge to me!). There's no code
coverage on this app.

# Architecture
![alt text](reviews.png "Architecture")

## API
Rails API for creating new reviews.

It uses PostgreSQL for persisting data and is seeded with the given example data
when the application is started for the first time.

When a new review is created, it publishes the review payload - including its
themes, categories and sentiment data - to the Kafka topic `reviews`

## Reviews Consumer
Golang service that consumes reviews from the Kafka broker and indexes it in
Elastiscsearch.

## Reviews Query Service
Golang service that queries Elasticsearch for reviews that attend a given
criteria, passed as a query from the client. Returns it's content as JSON.

## Dashboard
React app that provides a dashboard for analyzing review sentiments.
Charts are generated using Highcharts.

# Steps to Run
## Services
- clone the `reviews` repository and its submodules: `git clone --recursive git@gitlab.com:thiesen/reviews.git`
- cd into the `reviews` directory: `cd reviews`
- start all services with `docker-compose`: `docker-compose up -d`
- access the dashboard via http://localhost
- to create a new review, make a POST request to http://localhost/reviews with
  the following json body:

```json
{
   "review":{
      "comment":"exelent",
      "themes":[
         {
            "theme_id":6372,
            "sentiment":0
         }
      ],
      "created_at":"2019-07-18T23:28:36.000Z",
      "id":59458292
   }
}
```
**important: a theme with the given ID must exist.**

## Frontend
- cd into the `dashboard` directory: `cd dashboard`
- install the dependent packages and start the server: `yarn && yarn start`

# What is missing
- dashboard filters are not working properly: cannot revert filter selection
- extract all connection strings/references to environment variables
- encapsulation of the logging system in `reviews-services`
- refactor of the `elastic` repository in `reviews-services`
- refactor of the dashboard component in `dashboard`
- test coverage for `dashboard` and reviews service
